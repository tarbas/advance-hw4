///**
// * Класс, объекты которого описывают параметры гамбургера. 
// * 
// * @constructor
// * @param size        Размер
// * @param stuffing    Начинка
// * @throws {HamburgerException}  При неправильном использовании
// */

class HamburgerException {
    constructor(message) {
        this.message = message;
    }
}

class Hamburger {

    constructor(size, stuffing) {
        try {
            if (!size || !stuffing) {
                throw new HamburgerException("no size or stuffing given");
            } else if (!(size instanceof Object) || !(stuffing instanceof Object)) {
                throw new HamburgerException("ivalid type of size or stuffing given");
            } else if (size.name != Hamburger.SIZE_SMALL.name && size != Hamburger.SIZE_LARGE.name) {
                throw new HamburgerException("select correct size: Small or Large");
            } else if (stuffing.name != Hamburger.STUFFING_CHEESE.name && size.name != Hamburger.STUFFING_SALAD.name && size.name != Hamburger.STUFFING_POTATO.name) {
                throw new HamburgerException("select correct stuffing: Cheese, Salad or Potato");
            } else {
                this.size = size;
                this.stuffing = stuffing;
                this.toppings = [];
            }
        } catch (e) {
            console.log(e.message);
        }
    }

    /* Размеры, виды начинок и добавок */

    //    вариант первый константы:

    static get SIZE_SMALL() {
        const SIZE_SMALL = {
            name: 'SIZE_SMALL',
            price: 50,
            calories: 20
        };
        return SIZE_SMALL;
    }
    //    вариант второй константы:

    static get SIZE_LARGE() {
        return Object.freeze({
            name: 'SIZE_LARGE',
            price: 100,
            calories: 40
        });
    }

    static get STUFFING_CHEESE() {
        return Object.freeze({
            name: 'STUFFING_CHEESE',
            price: 10,
            calories: 20
        });
    }

    static get STUFFING_SALAD() {
        return Object.freeze({
            name: 'STUFFING_SALAD',
            price: 20,
            calories: 5
        });
    }

    static get STUFFING_POTATO() {
        return Object.freeze({
            name: 'STUFFING_SALAD',
            price: 15,
            calories: 10
        });
    }

    static get TOPPING_MAYO() {
        return Object.freeze({
            name: 'TOPPING_MAYO',
            price: 20,
            calories: 5
        });

    }
    static get TOPPING_SPICE() {
        return Object.freeze({
            name: 'TOPPING_SPICE',
            price: 15,
            calories: 0
        });
    }

    /**
     * Узнать цену гамбургера
     * @return {Number} Цена в тугриках
     */

    calculatePrice() {
        let price = 0;
        for (let key in this) {
            if (this[key].price) {
                price += this[key]["price"];
            }
        }
        if (this.toppings) {
            for (let i = 0; i < this.toppings.length; i++) {
                price += this.toppings[i].price;
            }
        }
        return price;
    }

    /*
     * Добавить добавку к гамбургеру. Можно добавить несколько
     * добавок, при условии, что они разные.
     * 
     * @param topping     Тип добавки
     * @throws {HamburgerException}  При неправильном использовании
     */

    addTopping(topping) {
        try {
            if (!topping) {
                throw new HamburgerException("no topping given");
            } else if (topping.name != Hamburger.TOPPING_MAYO.name && topping.name != Hamburger.TOPPING_SPICE.name) {
                throw new HamburgerException("select correct topping: Mayo or Spice");
            } else if (this.toppings.some((item) => item.name == topping.name)) {
                throw new HamburgerException("topping already given");
            } else {
                this.toppings.push(topping);
            }
            
        } catch (e) {
            console.log(e.message);
        }
    }

    /**
     * Убрать добавку, при условии, что она ранее была 
     * добавлена.
     * 
     * @param topping   Тип добавки
     * @throws {HamburgerException}  При неправильном использовании
     */
    removeTopping(topping) {
        try {
            if (!topping) {
                throw new HamburgerException("no topping specified");
            } else if (this.toppings.every((item) => item.name !== topping.name)) {
                throw new HamburgerException("this topping wasn't added");
            } else {
                this.toppings = this.toppings.filter((item) => item.name !== topping.name);

            }
        } catch (e) {
            console.log(e.message);
        }
    }

    /**
     * Узнать размер гамбургера
     */
    getSize() {
        return this.size;
    }



    /**
     * Получить список добавок.
     *
     * @return {Array} Массив добавленных добавок, содержит константы
     *                 Hamburger.TOPPING_*
     */
    getToppings() {
        return this.toppings;
    }

    /**
     * Узнать калорийность
     * @return {Number} Калорийность в калориях
     */
    calculateCalories() {
        let caloriesValue = 0;
        for (let key in this) {
            if (this[key].calories) {
                caloriesValue += this[key]["calories"];
            }
        }
        if (this.toppings) {
            for (let i = 0; i < this.toppings.length; i++) {
                caloriesValue += this.toppings[i].calories;
            }
        }
        return caloriesValue;
    }
}


// маленький гамбургер с начинкой из сыра
let ham1 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

// добавка из майонеза
ham1.addTopping(Hamburger.TOPPING_SPICE);
////
ham1.addTopping(Hamburger.TOPPING_SPICE); // ->проверка 
//
//// спросим сколько там калорий
console.log("Calories: %f", ham1.calculateCalories());
// сколько стоит
console.log("Price: %f", ham1.calculatePrice());
//// я тут передумал и решил добавить еще приправу
ham1.addTopping(Hamburger.TOPPING_MAYO);
//ham1.addTopping(Hamburger.TOPPING_SPICE);

console.log(ham1.toppings);
// А сколько теперь стоит? 
console.log("Price with sauce: %f", ham1.calculatePrice());
//// Проверить, большой ли гамбургер? 
console.log("Is hamburger large: %s", ham1.getSize() === Hamburger.SIZE_LARGE); // -> false
//// Убрать добавку
ham1.removeTopping(Hamburger.TOPPING_MAYO);
ham1.removeTopping(Hamburger.TOPPING_SPICE); // ->проверка 
ham1.removeTopping(Hamburger.TOPPING_SPICE); // ->проверка 
console.log(ham1.toppings);
//ham1.addTopping(Hamburger.TOPPING_SPICE);
//ham1.addTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", ham1.getToppings().length); // 1
